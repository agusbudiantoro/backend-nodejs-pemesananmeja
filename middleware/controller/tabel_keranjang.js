const connection = require('../../koneksi');
const mysql = require('mysql');
const response = require('../../res');
const path = require("path");
const pathHapus = './upload/images_barang/';
const fs = require('fs');

exports.tambahBarangInKeranjang = function (req, res) {
    var data = {
        id_barang: req.body.id_barang,
        qty: req.body.qty,
        total_harga: req.body.total_harga,
        id_konsumen: req.body.id_konsumen,
    }

    var query = "INSERT INTO ?? SET ?";
    var table = ["data_barang_keranjang"];

    query = mysql.format(query, table);
    connection.query(query, data, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "barang gagal tersimpan" })
        } else {
            res.json({
                id_barang: req.body.id_barang,
                qty: req.body.qty,
                total_harga: req.body.total_harga,
                id_konsumen:req.body.id_konsumen
            });
        }
    });
}

// tampil semua barang
exports.tampilBarangKeranjangByIdKonsumen = async function (req, res) {

    var hasilUser = new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["data_barang_keranjang","id_konsumen",req.body.id_konsumen];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
            } else {
                console.log("cek barang");
                resolve(rows);
            }
        });
    });
    var hasilKeranjang = await hasilUser;
    var tampungBarang = [];
    var tampungBarangSementara=[];
    
    for(var i=0;i<hasilKeranjang.length;i++){
        var hasilRes = new Promise(function (resolve, reject) {
            var query = "SELECT * FROM ?? WHERE ??=?";
            var table = ["data_barang","id_barang",hasilKeranjang[i].id_barang];
    
            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
                } else {
                    resolve(rows);
                }
            });
        });
        var tampungBarangSementara = await hasilRes;
        var data ={
            "id":hasilKeranjang[i].id,
            "id_barang":tampungBarangSementara[0].id_barang,
            "nama_barang":tampungBarangSementara[0].nama_barang,
            "harga_barang":tampungBarangSementara[0].harga,
            "total_harga":hasilKeranjang[i].total_harga,
            "qty":hasilKeranjang[i].qty,
            "stok":tampungBarangSementara[0].qty,
            "promo":tampungBarangSementara[0].promo,
            "harga_promo":tampungBarangSementara[0].harga_promo,
            "deskripsi":tampungBarangSementara[0].deskripsi,
            "kategori":tampungBarangSementara[0].kategori,
            "gambar":tampungBarangSementara[0].gambar
        }
        tampungBarang.push(data);
    }
    res.status(200).send({ value: tampungBarang.reverse() })
    
}

// hapus barang di keranjang

exports.hapusBarangInKeranjang = function(req, res){
    console.log(req.body);
    var query = "DELETE FROM ?? WHERE ??=?";
    var table = ["data_barang_keranjang","id",req.body.id];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal hapus barang dikeranjang" })
        } else {
            console.log("cek barang");
            response.ok("Berhasil Dihapus", res);
        }
    });
}
