const connection = require('../../koneksi');
const mysql = require('mysql');

exports.cekKetersediaanLapangan = function (req, res) {
    console.log(req.body);

    var query = "SELECT * FROM ?? WHERE ??=? AND ??=? AND ??=?";
    var table = ["jadwal_pesanan","paket_waktu",req.params.paket_waktu,"tanggal_pemesanan",req.params.tgl,"id_meja",req.params.id_meja];

    query = mysql.format(query, table);
    console.log(query);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: error })
        } else {
            console.log(rows);
            if(rows.length != 0){
                res.status(200).send({value:false});
            } else {
                res.status(200).send({value:true});
            }
        }
    });
}

exports.bookMeja = function (req, res) {
    console.log(req.body);

    let body ={
        id_tim: req.body.id_tim,
        nama_tim:req.body.nama_tim,
        no_telp:req.body.no_telp,
        tanggal_pemesanan:req.body.tanggal_pemesanan,
        paket_waktu:req.body.paket_waktu,
        id_meja:req.body.id_meja
    }

    var query = "INSERT INTO ?? SET ?";
    var table = ["jadwal_pesanan"];

    query = mysql.format(query, table);
    console.log(query);
    connection.query(query, body,function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: error })
        } else {
            res.status(200).send("berhasil booking");
        }
    });
}

exports.getAllBooking = function (req, res) {

    var query = "SELECT `jadwal_pesanan`.`id`, jadwal_pesanan.id_tim, jadwal_pesanan.nama_tim, jadwal_pesanan.no_telp, jadwal_pesanan.tanggal_pemesanan, jadwal_pesanan.id_meja, jadwal_pesanan.paket_waktu, tabel_meja.nomor_meja, kategori_paket_waktu.jam FROM jadwal_pesanan INNER JOIN tabel_meja ON jadwal_pesanan.id_meja=tabel_meja.id_meja INNER JOIN kategori_paket_waktu ON kategori_paket_waktu.id_paket_waktu=jadwal_pesanan.paket_waktu";
    var table = [];

    query = mysql.format(query, table);
    console.log(query);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: error })
        } else {
            res.status(200).json(rows);
        }
    });
}

exports.getAllBookingByIdTim = function (req, res) {

    var query = "SELECT `jadwal_pesanan`.`id`,kategori_paket_waktu.jam, jadwal_pesanan.id_tim, jadwal_pesanan.nama_tim, jadwal_pesanan.no_telp, jadwal_pesanan.tanggal_pemesanan, jadwal_pesanan.id_meja, jadwal_pesanan.paket_waktu, tabel_meja.nomor_meja FROM jadwal_pesanan INNER JOIN tabel_meja ON jadwal_pesanan.id_meja=tabel_meja.id_meja INNER JOIN kategori_paket_waktu ON kategori_paket_waktu.paket_jam=jadwal_pesanan.paket_waktu WHERE jadwal_pesanan.id_tim=?";
    var table = [req.params.id_tim];

    query = mysql.format(query, table);
    console.log(query);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: error })
        } else {
            res.status(200).json(rows);
        }
    });
}

exports.cekKetersediaanLapanganRes = function (req, res) {
    return new Promise(function (resolve, reject) {

        var query = "SELECT * FROM ?? WHERE ??=? AND ??=? AND ??=?";
        var table = ["jadwal_pesanan","paket_waktu",req.paket_waktu,"tanggal_pemesanan",req.tgl,"id_meja",req.id_meja];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: error })
            } else {
                if(rows.length != 0){
                    resolve(
                        {
                                value:true, 
                                data:{
                                    id:rows[0].id,
                                    id_tim:rows[0].id_tim,
                                    nama_tim:rows[0].nama_tim,
                                }
                        });
                } else {
                    resolve({value:false,data:{}});
                }
            }
        });
    });
}

exports.deleteJadwalPesanan = function (req, res) {
    var query1 = "DELETE FROM jadwal_pesanan WHERE id=?";
    var table1 = [req.params.id];

query1 = mysql.format(query1, table1);
connection.query(query1, function (error, rows) {
    if (error) {
        res.status(404).send("Gagal delete meja");
    } else {
        res.status(200).send("berhasil delete meja")
    }
})
}