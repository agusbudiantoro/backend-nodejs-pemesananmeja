const connection = require('../../koneksi');
const mysql = require('mysql');
const response = require('../../res');

exports.tampilStatus = function(req, res){
    var query = "SELECT status_pengiriman.id, status_pengiriman.id_riwayat_pemesanan, kategori_status_pengiriman.no_status, kategori_status_pengiriman.status FROM kategori_status_pengiriman INNER JOIN status_pengiriman ON kategori_status_pengiriman.no_status=status_pengiriman.status WHERE status_pengiriman.id_riwayat_pemesanan=?";
    var table = [req.body.id_riwayat_pemesanan];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan status" })
        } else {
            console.log("cek status");
            response.ok(rows, res);
        }
    });
}

exports.updateStatus = function(req,res){
    var query = "UPDATE status_pengiriman SET status=? WHERE id_riwayat_pemesanan=?";
    var table = [req.body.status,req.body.id_riwayat_pemesanan];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal update status" })
        } else {
            console.log("cek status");
            response.ok(rows, res);
        }
    });
}

// disiapkan
// dikirim
// sampai