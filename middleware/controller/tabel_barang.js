const connection = require('../../koneksi');
const mysql = require('mysql');
const response = require('../../res');
const path = require("path");
const pathHapus = './upload/images_barang/';
const fs = require('fs')

exports.tambahBarang = function (req, res) {
    console.log("cekbarangdagang");
    console.log(req.body);
    console.log(req.file.filename);
    console.log(req.file.originalname);
    console.log(req.file);
    var data = {
        nama_barang: req.body.nama_barang,
        qty: req.body.qty,
        harga: req.body.harga,
        gambar: req.file.filename,
        promo: req.body.promo,
        deskripsi: req.body.deskripsi,
        harga_promo: req.body.harga_promo,
        kategori: req.body.kategori
    }

    var query = "INSERT INTO ?? SET ?";
    var table = ["data_barang"];

    query = mysql.format(query, table);
    connection.query(query, data, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: error })
        } else {
            res.json({
                nama_barang: req.body.nama_barang,
                qty: req.body.qty,
                harga: req.body.harga,
                gambar: `${req.file.filename}`,
                promo: req.body.promo,
                deskripsi: req.body.deskripsi,
                harga_promo: req.body.harga_promo
            });
        }
    });
}

// tampil semua barang
exports.tampilBarang = function (req, res) {
    var query = "SELECT * FROM ??";
    var table = ["data_barang"];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
        } else {
            console.log("cek barang");
            response.ok(rows.reverse(), res);
        }
    });
}

//tampil barang promo
exports.tampilBarangPromo = function (req, res) {
    var query = "SELECT * FROM ?? WHERE ??!=0";
    var table = ["data_barang","promo"];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
        } else {
            console.log("cek barang");
            response.ok(rows, res);
        }
    });
}

// tampil barang rekomendasi
exports.tampilBarangRekomendasi = function (req, res) {
    var query = "SELECT * FROM ??";
    var table = ["data_barang"];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
        } else {
            if(rows.length != 0){
                let desc = rows.sort(function (a, b) {
                    return a.qty - b.qty
                });
                console.log("cek barang");
                response.ok(desc, res);
            }
        }
    });
}

//tampil barang no promo
exports.tampilBarangNoPromo = function (req, res) {
    var query = "SELECT * FROM ?? WHERE ??=0";
    var table = ["data_barang","promo"];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
        } else {
            console.log("cek barang");
            response.ok(rows, res);
        }
    });
}


//tampil barang by id
exports.tampilBarangById = function (req, res) {
    new Promise(function (resolve, reject) {

    var query = "SELECT * FROM ?? WHERE ??=0";
    var table = ["data_barang","id_barang", req.body.id_barang];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
        } else {
            console.log("cek barang");
            resolve(rows)
        }
    });
});
}

//tampil barang by kategori
exports.tampilBarangByKategori = function (req, res) {
    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["data_barang","kategori",req.body.kategori];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
        } else {
            console.log("cek barang");
            response.ok(rows, res);
        }
    });
}

//hapus barang
exports.hapusBarang = function (req, res) {
    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["data_barang", "id_barang", req.body.id_barang];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            response.failed("gagal", res);
        } else {
            if (rows.length == 1) {
                const path_foto=rows[0].gambar;
                var query1 = "DELETE FROM ?? WHERE ??=?";
                var table1 = ["data_barang", "id_barang", req.body.id_barang];

                query1 = mysql.format(query1, table1);
                connection.query(query1, function (error, rows) {
                    if (error) {
                        response.failed("Gagal hapus data", res)
                    } else {
                        try {
                            fs.unlinkSync(pathHapus+path_foto);
                            //file removed
                            var query2 = "DELETE FROM ?? WHERE ??=?";
                            var table2 = ["data_barang_keranjang", "id_barang", req.body.id_barang];

                            query2 = mysql.format(query2, table2);
                            connection.query(query2, function (error, rows) {
                                if (error) {
                                    response.failed("Gagal hapus data", res)
                                } else {
                                    try {
                                        var query3 = "DELETE FROM ?? WHERE ??=?";
                                        var table3 = ["riwayat_pesanan", "id_barang", req.body.id_barang];

                                        query3 = mysql.format(query3, table3);
                                        connection.query(query3, function (error, rows) {
                                            if (error) {
                                                response.failed("Gagal hapus data", res)
                                            } else {
                                                try {
                                                    //file removed
                                                } catch(err) {
                                                    console.error(err)
                                                }
                                                response.ok(rows, res);
                                            }
                                        })
                                    } catch(err) {
                                        res.status(401).send({message:error});
                                    }
                                }
                            })
                          } catch(err) {
                            res.status(401).send({message:error});
                          }
                    }
                })
            }
        }
    });
}


//edit barang
exports.editBarang = function (req, res, next) {
    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["data_barang", "id_barang", req.body.id_barang];
    query = mysql.format(query, table);
    console.log(req.body);
    console.log(req.file);
    connection.query(query, function (error, rows) {
        if (error) {
            response.failed("gagal", res);
        } else {
            if (rows.length == 1) {
                const path_foto=rows[0].gambar;
                if(req.file != undefined){
                    var query1 = "UPDATE data_barang SET nama_barang=?, qty=?, harga=?, gambar=?, promo=?, deskripsi=?, harga_promo=?, kategori=? WHERE id_barang=?";
                    var table1 = [req.body.nama_barang, req.body.qty, req.body.harga, req.file.filename, req.body.promo, req.body.deskripsi, req.body.harga_promo, req.body.kategori, req.body.id_barang];
                } else{
                    var query1 = "UPDATE data_barang SET nama_barang=?, qty=?, harga=?, promo=?, deskripsi=?, harga_promo=?, kategori=? WHERE id_barang=?";
                    var table1 = [req.body.nama_barang, req.body.qty, req.body.harga, req.body.promo, req.body.deskripsi, req.body.harga_promo, req.body.kategori, req.body.id_barang];
                }
                query1 = mysql.format(query1, table1);
                connection.query(query1, function (error, rows) {
                    if (error) {
                        response.failed("Gagal hapus data", res)
                    } else {
                        try {
                            if(req.file != undefined){
                                fs.unlinkSync(pathHapus+path_foto);
                            } else{
                                console.log("tidak ubah gambar");
                            }
                            //file removed
                          } catch(err) {
                            console.error(err)
                          }
                        response.ok(rows.message, res);
                    }
                })
            }
        }
    });
}