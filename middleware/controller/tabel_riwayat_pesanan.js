const connection = require('../../koneksi');
const mysql = require('mysql');
const response = require('../../res');
const path = require("path");
const pathHapus = './upload/images_barang/';
const fs = require('fs');
const axios = require('axios');
const qs = require('qs');
const tabelBarang = require('./tabel_barang');

//pembayaran
exports.tambahRiwayatPesanan = async function (req, res) {
    // mencari user
    var hasilUser = new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["user", "id", req.body.id_konsumen];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal mencari user" })
            } else {
                if (rows.length != 0) {
                    resolve(rows)
                } else {
                    res.status(404).send({ auth: false, message: "user yang dipilih tidak ada" })
                }
            }
        });
    });
    var hasilFinalUser = await hasilUser;

    // ambil kategori by id
    var hasilGetKategori = new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["metode_pembayaran", "id_metode_pembayaran", req.body.metode_pembayaran];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal mencari kategori" })
            } else {
                if (rows.length != 0) {
                    resolve(rows)
                } else {
                    res.status(404).send({ auth: false, message: "metode pembayaran yang dipilih tidak ada" })
                }
            }
        });
    });
    var hasilFinalKategori = await hasilGetKategori;

    // mencari id transaksi terakhir
    var hasilGetTransaksi = new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ??";
        var table = ["id_midtrans",];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal menampilkan data id transaksi" })
            } else {
                if (rows.length != 0) {
                    resolve(rows)
                } else {
                    res.status(404).send({ auth: false, message: "riwayat id transaksi tidak ada" });
                }
            }
        });
    });
    var hasilFinalTransaksi = await hasilGetTransaksi;
    let desc = hasilFinalTransaksi.sort(function (a, b) {
        return b.id_midtrans - a.id_midtrans
    });

    // mencari jumlah stok barang
    var hasilGetStokBarang = new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["data_barang", "id_barang", req.body.id_barang];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal menampilkan data barang" })
            } else {
                if (rows.length != 0) {
                    resolve(rows)
                } else {
                    res.status(404).send({ auth: false, message: "data barang tidak ditemukan" })
                }
            }
        });
    });
    var hasilFinalGetStokBarang = await hasilGetStokBarang;
    var jumlahStokSekarang = hasilFinalGetStokBarang[0].qty - req.body.qty;
    if (jumlahStokSekarang < 0) {
        res.status(405).send({ auth: false, message: "Stok Barang Tidak Cukup" })
    } else if (jumlahStokSekarang >= 0) {
        // edit stock barang
        var hasilPutStokBarang = new Promise(function (resolve, reject) {
            var query = "UPDATE data_barang SET qty=? WHERE id_barang=?";
            var table = [jumlahStokSekarang, req.body.id_barang];

            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal update stok barang" })
                } else {
                    resolve(rows);
                }
            });
        });
        var hasilFinalPutStokBarang = await hasilPutStokBarang;

        var dataShopee = {
            payment_type: "shopeepay",
            transaction_details: {
                order_id: "cfbook"+(desc[0].id_midtrans + 1),
                // order_id: "mg214",
                gross_amount: req.body.total_harga
            },
            customer_details: {
                first_name: hasilFinalUser[0].username,
                last_name: "",
                email: hasilFinalUser[0].email,
                phone: hasilFinalUser[0].no_hp,
                billing_address: {
                    address: req.body.alamat,
                },
            },
            shopeepay: {
                callback_url: "https://midtrans.com/"
            }
        }

        var dataGopay = {
            payment_type: "gopay",
            transaction_details: {
                order_id: "cfbook"+(desc[0].id_midtrans + 1),
                // order_id: "mg234",
                gross_amount: req.body.total_harga
            },
            customer_details: {
                first_name: hasilFinalUser[0].username,
                last_name: "",
                email: hasilFinalUser[0].email,
                phone: hasilFinalUser[0].no_hp,
                billing_address: {
                    address: req.body.alamat,
                },
            },
            gopay: {
                enable_callback: true,
                callback_url: "someapps://callback"
            }
        }
        let dataPembayaran = {};
        if (req.body.metode_pembayaran == "1") {
            dataPembayaran = dataShopee;
        } else if (req.body.metode_pembayaran == "2") {
            dataPembayaran = dataGopay;
        }
        //post data ke midtrans
        let hasilMidtrans = new Promise(function (resolve, reject) {
            axios.defaults.headers.common['Authorization'] = "Basic U0ItTWlkLXNlcnZlci02VjcyaXg2cW1TNFdXQVVKR01NenpMd1o6";
            axios.post('https://api.sandbox.midtrans.com/v2/charge', dataPembayaran,)
                .then(function (response) {
                    // handle success
                    resolve(response.data)
                })
                .catch(function (error) {
                    // handle error
                    res.status(404).send({ auth: false, message: "gagal post ke midtrans" })
                });
        });
        let hasilFinalMidtrans = await hasilMidtrans;
        var dataRiwayat={};
        //post ke riwayat pesanan
        let resPostRiwayatPemesanan = new Promise(function (resolve, reject) {
            dataRiwayat = {
                id_konsumen: req.body.id_konsumen,
                qty: req.body.qty,
                harga: req.body.harga,
                total_harga: req.body.total_harga,
                waktu_pengiriman: "",
                nama_penerima: hasilFinalUser[0].username,
                alamat: req.body.alamat,
                no_hp: hasilFinalUser[0].no_hp,
                id_barang: req.body.id_barang,
                nama_barang: req.body.nama_barang,
                waktu_pemesanan: hasilFinalMidtrans.transaction_time,
                metode_pembayaran: req.body.metode_pembayaran,
                estimasi_sampai: "2 Hari Setelah Pengiriman",
                midtrans_id_transaksi: hasilFinalMidtrans.order_id,
                midtrans_id_pemesanan: hasilFinalMidtrans.transaction_id,
                status_pengiriman: 0,
                status_kirim: false,
                link_pembayaran: (req.body.metode_pembayaran == 1) ? hasilFinalMidtrans.actions[0].url : hasilFinalMidtrans.actions[1].url
            }

            var query = "INSERT INTO ?? SET ?";
            var table = ["riwayat_pesanan"];

            query = mysql.format(query, table);
            connection.query(query, dataRiwayat, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: 'barang gagal tersimpan ' + error });
                } else {
                    resolve(rows);
                }
            });
        });
        let hasilResRiwayatPemesanan = await resPostRiwayatPemesanan;

        //post ke tabel id_midtrans
        let resPostIdMidtrans = new Promise(function (resolve, reject) {
            var data = {
                id_midtrans: hasilFinalMidtrans.order_id.substring(4),
                id_midtrans_transaksi: hasilFinalMidtrans.transaction_id
            }

            var query = "INSERT INTO ?? SET ?";
            var table = ["id_midtrans"];

            query = mysql.format(query, table);
            connection.query(query, data, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: 'id gagal tersimpan ' + error });
                } else {
                    resolve(rows);
                }
            });
        });
        let hasilFinalPostIdMidtrans = await resPostIdMidtrans;

        // ambil riwayat pesanan by id
        let getRiwayatPemesanan = new Promise(function (resolve, reject) {
            var query = "SELECT * FROM ?? WHERE ??=?";
            var table = ["riwayat_pesanan", "midtrans_id_pemesanan", hasilFinalMidtrans.transaction_id];

            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal mencari pesanan" })
                } else {
                    if (rows.length != 0) {
                        resolve(rows)
                    } else {
                        res.status(404).send({ auth: false, message: "riwayat pesanan yang dipilih tidak ada" })
                    }
                }
            });
        });
        let finalGetRiwayatPemesanan = await getRiwayatPemesanan;

        //post status
        let postStatus = new Promise(function (resolve, reject) {
            var data = {
                id_riwayat_pemesanan: finalGetRiwayatPemesanan[0].no_riwayat_pesanan,
                status: 1
            }

            var query = "INSERT INTO ?? SET ?";
            var table = ["status_pengiriman"];

            query = mysql.format(query, table);
            connection.query(query, data, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "status gagal tersimpan" })
                } else {
                    resolve(rows);
                }
            });
        });

        let finalPostStatus = await postStatus;

        if (req.body.id_keranjang != 'null') {
            let hapusBarangKeranjang = new Promise(function (resolve, reject) {
                var query = "DELETE FROM ?? WHERE ??=?";
                var table = ["data_barang_keranjang", "id", req.body.id_keranjang];

                query = mysql.format(query, table);
                connection.query(query, function (error, rows) {
                    if (error) {
                        res.status(404).send({ auth: false, message: "gagal hapus barang dikeranjang" })
                    } else {
                        res.status(200).send({
                            link:dataRiwayat.link_pembayaran
                        });
                    }
                });
            });
        } else {
            res.status(200).send({
                link:dataRiwayat.link_pembayaran
            });
        }
    }

}

//get pembayaran by id konsumen
exports.tampilPesananByIdKonsumen = async function (req, res) {
    var hasilGetHistoryPesanan = new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["riwayat_pesanan", "id_konsumen", req.body.id_konsumen];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal menampilkan data riwayat pesanan" })
            } else {
                if (rows.length != 0) {
                    resolve(rows);
                } else {
                    res.status(404).send({ auth: false, message: "data riwayat pesanan tidak ditemukan" });
                }
            }
        });
    });

    var finalHasilGetRiwayatPesanan = await hasilGetHistoryPesanan;
    var tampungData = [];
    for (var i = 0; i < finalHasilGetRiwayatPesanan.length; i++) {
        var hasilGetBarang = new Promise(function (resolve, reject) {

            var query = "SELECT * FROM ?? WHERE ??=?";
            var table = ["data_barang", "id_barang", finalHasilGetRiwayatPesanan[i].id_barang];

            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
                } else {
                    resolve(rows)
                }
            });
        });
        var finalGetBarang = await hasilGetBarang;

        //get data status dari midtrans
        let hasilMidtrans = new Promise(function (resolve, reject) {
            axios.defaults.headers.common['Authorization'] = "Basic U0ItTWlkLXNlcnZlci02VjcyaXg2cW1TNFdXQVVKR01NenpMd1o6";
            axios.get('https://api.sandbox.midtrans.com/v2/' + finalHasilGetRiwayatPesanan[i].midtrans_id_pemesanan + '/status')
                .then(function (response) {
                    // handle success
                    resolve(response.data)
                })
                .catch(function (error) {
                    // handle error
                    res.status(404).send({ auth: false, message: "gagal post ke midtrans" })
                });
        });
        let hasilFinalMidtrans = await hasilMidtrans;

        // ambil metode pembayaran by id
        var hasilGetMetodePembayaran = new Promise(function (resolve, reject) {
            var query = "SELECT * FROM ?? WHERE ??=?";
            var table = ["metode_pembayaran", "id_metode_pembayaran", finalHasilGetRiwayatPesanan[i].metode_pembayaran];

            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal mencari metode pembayaran" })
                } else {
                    if (rows.length != 0) {
                        resolve(rows)
                    } else {
                        res.status(404).send({ auth: false, message: "metode pembayaran yang dipilih tidak ada" })
                    }
                }
            });
        });
        var hasilFinalMetodePembayaran = await hasilGetMetodePembayaran;
        var statusKirim = finalHasilGetRiwayatPesanan[i].status_kirim;
        var status_pengiriman = finalHasilGetRiwayatPesanan[i].status_pengiriman;
        if (finalHasilGetRiwayatPesanan[i].status_kirim == false) {
            if (hasilFinalMidtrans.status_code == 200) {
                // jika sudah dibayar status kirim akan menjadi 1 || disiapkan
                var hasilPutStatusKirimInRiwayatPesanan = new Promise(function (resolve, reject) {
                    var query = "UPDATE riwayat_pesanan SET status_kirim=?, status_pengiriman=? WHERE no_riwayat_pesanan=?";
                    var table = [true, 1, finalHasilGetRiwayatPesanan[i].no_riwayat_pesanan];

                    query = mysql.format(query, table);
                    connection.query(query, function (error, rows) {
                        if (error) {
                            res.status(404).send({ auth: false, message: "gagal update status kirim" })
                        } else {
                            resolve({ status_kirim: true, status_pengiriman: 1 });
                        }
                    });
                });
                var hasilFinalPutStatusKirimInRiwayarPesanan = await hasilPutStatusKirimInRiwayatPesanan;
                statusKirim = hasilFinalPutStatusKirimInRiwayarPesanan.status_kirim;
                status_pengiriman = hasilFinalPutStatusKirimInRiwayarPesanan.status_pengiriman;
            }
        }

        tampungData.push({
            status_kirim: (statusKirim == true)?1:0,
            status_pengiriman: status_pengiriman,
            no_riwayat_pesanan: finalHasilGetRiwayatPesanan[i].no_riwayat_pesanan,
            id_konsumen: finalHasilGetRiwayatPesanan[i].id_konsumen,
            waktu_pengiriman: finalHasilGetRiwayatPesanan[i].waktu_pengiriman,
            nama_penerima: finalHasilGetRiwayatPesanan[i].nama_penerima,
            alamat: finalHasilGetRiwayatPesanan[i].alamat,
            no_hp: finalHasilGetRiwayatPesanan[i].no_hp,
            id_barang: finalHasilGetRiwayatPesanan[i].id_barang,
            nama_barang: finalHasilGetRiwayatPesanan[i].nama_barang,
            qty: finalHasilGetRiwayatPesanan[i].qty,
            harga: finalHasilGetRiwayatPesanan[i].harga,
            waktu_pemesanan: finalHasilGetRiwayatPesanan[i].waktu_pemesanan,
            metode_pembayaran: finalHasilGetRiwayatPesanan[i].metode_pembayaran,
            nama_metode_pembayaran: hasilFinalMetodePembayaran[0].nama_pembayaran,
            logo_metode_pembayaran: hasilFinalMetodePembayaran[0].logo,
            estimasi_sampai: finalHasilGetRiwayatPesanan[i].estimasi_sampai,
            midtrans_id_transaksi: finalHasilGetRiwayatPesanan[i].midtrans_id_transaksi,
            midtrans_id_pemesanan: finalHasilGetRiwayatPesanan[i].midtrans_id_pemesanan,
            total_harga: finalHasilGetRiwayatPesanan[i].total_harga,
            image: finalGetBarang[0].gambar,
            status_code_pembayaran: hasilFinalMidtrans.status_code,
            link_pembayaran:finalHasilGetRiwayatPesanan[i].link_pembayaran,
            waktu_pembayaran: (hasilFinalMidtrans.status_code == 200) ? hasilFinalMidtrans.settlement_time : "",
            status: (hasilFinalMidtrans.status_code == 200) ? "Dibayar" : (hasilFinalMidtrans.status_code == 407) ? "Kadaluarsa" : "Menunggu Pembayaran"
        });
    }

    res.status(200).send({ value: tampungData.reverse() });
};

//get all pembayaran
exports.tampilAllPesanan = async function (req, res) {
    var hasilGetHistoryPesanan = new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ?? WHERE status_kirim=?";
        var table = ["riwayat_pesanan", true];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal menampilkan data riwayat pesanan" })
            } else {
                if (rows.length != 0) {
                    resolve(rows.reverse());
                } else {
                    res.status(404).send({ auth: false, message: "data riwayat pesanan tidak ditemukan" });
                }
            }
        });
    });

    var finalHasilGetRiwayatPesanan = await hasilGetHistoryPesanan;
    var tampungData = [];
    for (var i = 0; i < finalHasilGetRiwayatPesanan.length; i++) {
        var hasilGetBarang = new Promise(function (resolve, reject) {

            var query = "SELECT * FROM ?? WHERE ??=?";
            var table = ["data_barang", "id_barang", finalHasilGetRiwayatPesanan[i].id_barang];

            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal menampilkan barang" })
                } else {
                    resolve(rows)
                }
            });
        });
        var finalGetBarang = await hasilGetBarang;

        //get data status dari midtrans
        let hasilMidtrans = new Promise(function (resolve, reject) {
            axios.defaults.headers.common['Authorization'] = "Basic U0ItTWlkLXNlcnZlci02VjcyaXg2cW1TNFdXQVVKR01NenpMd1o6";
            axios.get('https://api.sandbox.midtrans.com/v2/' + finalHasilGetRiwayatPesanan[i].midtrans_id_pemesanan + '/status')
                .then(function (response) {
                    // handle success
                    resolve(response.data)
                })
                .catch(function (error) {
                    // handle error
                    res.status(404).send({ auth: false, message: "gagal post ke midtrans" })
                });
        });
        let hasilFinalMidtrans = await hasilMidtrans;

        // ambil metode pembayaran by id
        var hasilGetMetodePembayaran = new Promise(function (resolve, reject) {
            var query = "SELECT * FROM ?? WHERE ??=?";
            var table = ["metode_pembayaran", "id_metode_pembayaran", finalHasilGetRiwayatPesanan[i].metode_pembayaran];

            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal mencari metode pembayaran" })
                } else {
                    if (rows.length != 0) {
                        resolve(rows)
                    } else {
                        res.status(404).send({ auth: false, message: "metode pembayaran yang dipilih tidak ada" })
                    }
                }
            });
        });
        var hasilFinalMetodePembayaran = await hasilGetMetodePembayaran;
        var statusKirim = finalHasilGetRiwayatPesanan[i].status_kirim;
        var status_pengiriman = finalHasilGetRiwayatPesanan[i].status_pengiriman;
        if (finalHasilGetRiwayatPesanan[i].status_kirim == false) {
            if (hasilFinalMidtrans.status_code == 200) {
                // jika sudah dibayar status kirim akan menjadi 1 || disiapkan
                var hasilPutStatusKirimInRiwayatPesanan = new Promise(function (resolve, reject) {
                    var query = "UPDATE riwayat_pesanan SET status_kirim=?, status_pengiriman=? WHERE no_riwayat_pesanan=?";
                    var table = [true, 1, finalHasilGetRiwayatPesanan[i].no_riwayat_pesanan];

                    query = mysql.format(query, table);
                    connection.query(query, function (error, rows) {
                        if (error) {
                            res.status(404).send({ auth: false, message: "gagal update status kirim" })
                        } else {
                            resolve({ status_kirim: true, status_pengiriman: 1 });
                        }
                    });
                });
                var hasilFinalPutStatusKirimInRiwayarPesanan = await hasilPutStatusKirimInRiwayatPesanan;
                statusKirim = hasilFinalPutStatusKirimInRiwayarPesanan.status_kirim;
                status_pengiriman = hasilFinalPutStatusKirimInRiwayarPesanan.status_pengiriman;
            }
        }

        tampungData.push({
            status_kirim: (statusKirim == true)?1:0,
            status_pengiriman: status_pengiriman,
            no_riwayat_pesanan: finalHasilGetRiwayatPesanan[i].no_riwayat_pesanan,
            id_konsumen: finalHasilGetRiwayatPesanan[i].id_konsumen,
            waktu_pengiriman: finalHasilGetRiwayatPesanan[i].waktu_pengiriman,
            nama_penerima: finalHasilGetRiwayatPesanan[i].nama_penerima,
            alamat: finalHasilGetRiwayatPesanan[i].alamat,
            no_hp: finalHasilGetRiwayatPesanan[i].no_hp,
            id_barang: finalHasilGetRiwayatPesanan[i].id_barang,
            nama_barang: finalHasilGetRiwayatPesanan[i].nama_barang,
            qty: finalHasilGetRiwayatPesanan[i].qty,
            harga: finalHasilGetRiwayatPesanan[i].harga,
            waktu_pemesanan: finalHasilGetRiwayatPesanan[i].waktu_pemesanan,
            metode_pembayaran: finalHasilGetRiwayatPesanan[i].metode_pembayaran,
            nama_metode_pembayaran: hasilFinalMetodePembayaran[0].nama_pembayaran,
            logo_metode_pembayaran: hasilFinalMetodePembayaran[0].logo,
            estimasi_sampai: finalHasilGetRiwayatPesanan[i].estimasi_sampai,
            midtrans_id_transaksi: finalHasilGetRiwayatPesanan[i].midtrans_id_transaksi,
            midtrans_id_pemesanan: finalHasilGetRiwayatPesanan[i].midtrans_id_pemesanan,
            total_harga: finalHasilGetRiwayatPesanan[i].total_harga,
            image: finalGetBarang[0].gambar,
            status_code_pembayaran: hasilFinalMidtrans.status_code,
            waktu_pembayaran: (hasilFinalMidtrans.status_code == 200) ? hasilFinalMidtrans.settlement_time : "",
            status: (hasilFinalMidtrans.status_code == 200) ? "Dibayar" : (hasilFinalMidtrans.status_code == 407) ? "Kadaluarsa" : "Menunggu Pembayaran"
        })
    }

    res.status(200).send({ value: tampungData });
};

// hapus riwayat pesanan

exports.hapusRiwayatPesanan = async function (req, res) {
    // mencari jumlah stok barang
    var hasilGetStokBarang = new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["data_barang", "id_barang", req.body.id_barang];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal menampilkan data barang" })
            } else {
                if (rows.length != 0) {
                    resolve(rows)
                } else {
                    res.status(404).send({ auth: false, message: "data barang tidak ditemukan" })
                }
            }
        });
    });
    var hasilFinalGetStokBarang = await hasilGetStokBarang;
    var jumlahStokSekarang = hasilFinalGetStokBarang[0].qty + parseInt(req.body.qty);
    // edit stock barang
    var hasilPutStokBarang = new Promise(function (resolve, reject) {
        var query = "UPDATE data_barang SET qty=? WHERE id_barang=?";
        var table = [jumlahStokSekarang, req.body.id_barang];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal update stok barang" })
            } else {
                resolve(rows);
            }
        });
    });
    var hasilFinalPutStokBarang = await hasilPutStokBarang;

    var hasilDeleteRiwayat = new Promise(function (resolve, reject) {
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["riwayat_pesanan", "no_riwayat_pesanan", req.body.no_riwayat_pesanan];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal hapus riwayat pesanan" })
            } else {
                response.ok("Berhasil Dihapus", res);
            }
        });
    });
}

// kirim pesanan dan ambil barang

exports.updateStatusPesanan = async function (req, res) {
    var query = "UPDATE ?? SET status_pengiriman=? WHERE no_riwayat_pesanan=?";
        var table = ["riwayat_pesanan", parseInt(req.body.status_pengiriman)+1, req.body.no_riwayat_pesanan];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal update status pesanan" })
            } else {
                response.ok("Berhasil update pesanan", res);
            }
        });
}