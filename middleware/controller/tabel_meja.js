const connection = require('../../koneksi');
const mysql = require('mysql');
const tabel_jadwal_pesanan = require('../controller/tabel_jadwal_pesanan');

exports.tambahMeja = function (req, res) {
    
    var data = {
        nomor_meja: req.body.nomor_meja
    }

    var query = "INSERT INTO ?? SET ?";
    var table = ["tabel_meja"];

    query = mysql.format(query, table);
    connection.query(query, data, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: error })
        } else {
            res.status(200).send("berhasil menambahkan meja");;
        }
    });
}

exports.editMeja = function (req, res) {
                    var query1 = "UPDATE tabel_meja SET nomor_meja=? WHERE id_meja=?";
                    var table1 = [req.body.nomor_meja,req.body.id_meja];
        
                query1 = mysql.format(query1, table1);
                connection.query(query1, function (error, rows) {
                    if (error) {
                        res.status(404).send("Gagal edit meja");
                    } else {
                        res.status(200).send("berhasil edit meja")
                    }
                })
}

exports.deleteMeja = function (req, res) {
    var query1 = "DELETE FROM tabel_meja WHERE id_meja=?";
    var table1 = [req.params.id_meja];

query1 = mysql.format(query1, table1);
connection.query(query1, function (error, rows) {
    if (error) {
        res.status(404).send("Gagal delete meja");
    } else {
        res.status(200).send("berhasil delete meja")
    }
})
}

exports.tampilMeja = function (req, res) {
    var query = "SELECT * FROM ??";
    var table = ["tabel_meja"];

    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal menampilkan data" })
        } else {
            console.log("cek barang");
            res.status(200).json(rows);
        }
    });
}


exports.tampilAllMejaAndBooking = async function(req,res){

    let paket_waktu= await req.params.paket_waktu;
    let tgl = await req.params.tgl;

    var resMeja= await new Promise(function (resolve, reject) {
        var query = "SELECT * FROM ??";
        var table = ["tabel_meja"];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: "gagal menampilkan data" })
            } else {
                resolve(rows);
            }
        });
    });

    let body = [];
    let form={
        
    }

    for(var i=0;i<resMeja.length;i++){
        form = await {
            paket_waktu:paket_waktu,
            tgl:tgl,
            id_meja:resMeja[i].id_meja
        }
        let resStatus = await tabel_jadwal_pesanan.cekKetersediaanLapanganRes(form, res);
        body.push({
            "id_meja": resMeja[i].id_meja,
            "nomor_meja": resMeja[i].nomor_meja,
            "status":resStatus.value,
            "data":(resStatus.value != false)?resStatus.data:{
                "id": null,
                "id_tim": null,
                "nama_tim": ""
            }
        });
    }

    return res.status(200).json(body);
}