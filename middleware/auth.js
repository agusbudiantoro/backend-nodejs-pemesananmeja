const connection = require('../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../res');
const jwt = require('jsonwebtoken');
const config = require('../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");


// controller untuk register
exports.cekAkun = function cekMyAkun(){
    return function (req, res, next) {
    var post = {
        email: req.body.email
    }

    var query = "SELECT email FROM ?? WHERE ??=?";
    var table = ["user", "email", post.email];

    query = mysql.format(query, table);

    connection.query(query, post, function (error, rows) {
        if (error) {
            console.log(error);
        } else {
            if (rows.length == 0) {
                next();
            } else {
                response.failed("Email sudah terdaftar!", res);
            }
        }
    })
}
}

exports.register = function (req, res) {
    bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
        console.log("cek log");
        console.log(req.body);
        var post = {
            username: req.body.username,
            email: req.body.email,
            password: hash,
            role: req.body.role,
            tanggal_daftar: new Date(),
            alamat: req.body.alamat,
            no_hp: req.body.no_hp,
            // foto: `${req.file.fieldname}_${Date.now()}${path.extname(req.file.originalname)}`
        }

        var query = "INSERT INTO ?? SET ?";
        var table = ["user"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                console.log(error);
            } else {
                response.ok("Berhasil Menambahkan Data", res);
            }
        });
    })
}

// controller untuk login
exports.login = function (req, res) {
    var post = {
        password: req.body.password,
        email: req.body.email
    }

    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["user", "email", post.email];

    query = mysql.format(query, table);

    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({auth:false,message:"akun tidak terdaftar"})
        } else {
            if (rows.length == 1) {
                    bcrypt.compare(post.password, rows[0].password, function (err, result) {
                        if (result == true) {
                            var token = jwt.sign({ rows }, config.secret, {
                                expiresIn: 5760
                            });
                            id_user = rows[0].id;
                            role_user = rows[0].role;
                            userName = rows[0].username;
                            const no_hp=rows[0].no_hp;
                            const alamat=rows[0].alamat;
                            const email =rows[0].email;
                            const foto=rows[0].foto;
                            var data = {
                                id_user: id_user,
                                role: role_user,
                                access_token: token,
                                ip_address: ip.address()
                            }
                            console.log(data.no_hp);
                            var query = "INSERT INTO ?? SET ?";
                            var table = ["akses_token"];
    
                            query = mysql.format(query, table);
                            connection.query(query, data, function (error, rows) {
                                if (error) {
                                    res.status(404).send({auth:false,message:"akun gagal teregristrasi"})
                                } else {
                                    res.json({
                                        success: true,
                                        message: "token jwt tergenerate",
                                        token: token,
                                        currUser: data.id_user,
                                        role: data.role,
                                        no_hp:no_hp,
                                        foto:foto,
                                        alamat:alamat,
                                        email:email,
                                        userName:userName
                                    });
                                }
                            });
                        } else {
                            res.status(401).send({auth:false,message:"password salah"});
                        }
                    });
            } else {
                res.status(401).send({auth:false,message:"email salah"});
            }
        }
    });
}

// controller ganti password
exports.gantiPass = function (req, res) {
    var post = {
        newPassword: req.newPassword,
        password: req.body.password,
        email: req.body.email
    }

    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["user", "email", post.email];

    query = mysql.format(query, table);

    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({auth:false,message:"akun tidak terdaftar"})
        } else {
            if (rows.length == 1) {
                    bcrypt.compare(post.password, rows[0].password, function (err, result) {
                        if (result == true) {
                            id_user = rows[0].id;
                            role_user = rows[0].role;
                            userName = rows[0].username;
                            const no_hp=rows[0].no_hp;
                            const alamat=rows[0].alamat;
                            const email =rows[0].email;
                            const foto=rows[0].foto;
                            var data = {
                                id_user: id_user,
                                ip_address: ip.address()
                            }
                            bcrypt.hash(req.body.newPassword, saltRounds, function (err, hash) {
                                if(err){
                                    res.status(404).send({auth:false,message:"gagal terenkripsi"});
                                } else {
                                    console.log(data.no_hp);
                                    var query = "UPDATE user SET password=? WHERE id=?";
                                    var table = [hash,data.id_user];
                                    
                                    query = mysql.format(query, table);
                                    console.log(query);
                                    connection.query(query, data, function (error, rows) {
                                        if (error) {
                                            res.status(404).send({auth:false,message:"akun gagal ganti password"})
                                        } else {
                                            res.status(200).send({message:"berhasil ganti password"});
                                        }
                                    });
                                }
                            });
                        } else {
                            res.status(401).send({auth:false,message:"password salah"});
                        }
                    });
            } else {
                res.status(401).send({auth:false,message:"email salah"});
            }
        }
    });
}

// controller cek
exports.cek = function(req, res){
    console.log("cekk");
    console.log(req.file);
}

exports.cekToken = function (req, res) {
    response.ok("Token Tersedia", res);
}