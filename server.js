const express = require('express');
const app = express();
const bodyParser =  require('body-parser');
const PORT = process.env.MYSQL_PORT || 3002;
const koneksi = require('./koneksi');
const morgan = require('morgan');

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use('/auth', require('./middleware'));
app.use('/gambar', express.static('./upload/images_barang'),(req,res)=>{
    console.log("cek");
});
app.use('/logo', express.static('./upload/images_logo_pembayaran'),(req,res)=>{
    console.log("cek");
});
var routes = require("./router");
routes(app);
app.listen(PORT, () => {
    console.log(PORT);
    console.log(`Server started on port`);
});