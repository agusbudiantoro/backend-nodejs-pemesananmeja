'use strict';

module.exports = function(app){
    var iam = require('./controller');
    app.route('/',).get(iam.index);

    app.route('/getAll').get(iam.tampilAll);
}